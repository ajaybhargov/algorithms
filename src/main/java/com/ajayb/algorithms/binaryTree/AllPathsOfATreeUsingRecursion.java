package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;
import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTree;

import java.util.ArrayList;

/**
 * Responsible for printing all the paths of a tree using recursion
 *
 * @author : ajay.b
 */
public class AllPathsOfATreeUsingRecursion {
    public void displayAllPaths(BinarySearchTree binarySearchTree) {
        ArrayList<ArrayList<Integer>> listOfAllPaths = new ArrayList<ArrayList<Integer>>();
        Node node = binarySearchTree.getNode();
        if (node != null) {
            displayAllPathsUsingRecursion(node, listOfAllPaths, new ArrayList<Integer>());
        }
        System.out.println("listOfAllPaths = " + listOfAllPaths);
    }

    private void displayAllPathsUsingRecursion(Node node, ArrayList<ArrayList<Integer>> listOfAllPaths, ArrayList<Integer> elementsInCurrentPath) {
        if (node == null) {
            ArrayList<Integer> elementsInThePath = new ArrayList<Integer>();
            elementsInThePath.addAll(elementsInCurrentPath);
            listOfAllPaths.add(elementsInThePath);
            return;
        } else if (node.getLeft() == null && node.getRight() == null) {
            ArrayList<Integer> elementsInThePath = new ArrayList<Integer>();
            elementsInThePath.addAll(elementsInCurrentPath);
            elementsInThePath.add(node.getValue());
            listOfAllPaths.add(elementsInThePath);
            return;
        }

        ArrayList<Integer> currentPath = new ArrayList<Integer>();
        currentPath.addAll(elementsInCurrentPath);
        currentPath.add(node.getValue());
        displayAllPathsUsingRecursion(node.getLeft(), listOfAllPaths, currentPath);
        displayAllPathsUsingRecursion(node.getRight(), listOfAllPaths, currentPath);
        return;
    }
}
