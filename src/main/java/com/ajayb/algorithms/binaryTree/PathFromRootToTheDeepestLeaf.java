package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Path from root to the deepest leaf.
 *
 * @author : ajay.b
 */
public class PathFromRootToTheDeepestLeaf {
    public void print(BinaryTree binaryTree) {
        List<Integer> elements = getAllElementsFromPathToTheDeepestLeaf(binaryTree.getNode());
        System.out.println("elements = " + elements);
    }

    private List<Integer> getAllElementsFromPathToTheDeepestLeaf(Node node) {
        ArrayList<Integer> elements = new ArrayList<Integer>();
        if (node.getLeft() == null && node.getRight() == null) {
            elements.add(node.getValue());
            return elements;
        }
        List<Integer> elementsFromLeftTree = new ArrayList<Integer>();
        if (node.getLeft() != null) {
            elementsFromLeftTree = getAllElementsFromPathToTheDeepestLeaf(node.getLeft());
        } else {
            elementsFromLeftTree.add(node.getValue());
        }
        List<Integer> elementsFromRightTree = new ArrayList<Integer>();
        if (node.getRight() != null) {
            elementsFromRightTree = getAllElementsFromPathToTheDeepestLeaf(node.getRight());
        } else {
            elementsFromRightTree.add(node.getValue());
        }

        if (elementsFromLeftTree.size() > elementsFromRightTree.size()) {
            elements.add(node.getValue());
            elements.addAll(elementsFromLeftTree);
        } else {
            elements.add(node.getValue());
            elements.addAll(elementsFromRightTree);
        }
        return elements;
    }

}
