package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author : ajay.b
 */
public class BinaryTree {
    private Node node;

    public BinaryTree(Node node) {
        this.node = node;
    }

    @Override
    public String toString() {
        BinaryTreePrinter.printNode(node);
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public Node getNode() {
        return node;
    }
}
