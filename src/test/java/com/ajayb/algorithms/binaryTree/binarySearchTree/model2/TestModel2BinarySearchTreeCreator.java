package com.ajayb.algorithms.binaryTree.binarySearchTree.model2;

import org.testng.annotations.Test;

import java.util.ArrayList;

/**
 * @author : ajay.b
 */
public class TestModel2BinarySearchTreeCreator {
    /**
     * Test should successfully create a model 2 BinarySearchTree
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyCreateModel2BinarySearchTree() throws Exception {
        createModel2BinarySearchTree(10, 5, 8, 9);
    }

    private void createModel2BinarySearchTree(Integer... listOfElements) {
        ArrayList<Integer> elements = new ArrayList<Integer>();
        for (int i = 0; i < listOfElements.length; i++) {
            elements.add(listOfElements[i]);
        }
        Model2BinarySearchTree binarySearchTree = Model2BinarySearchTreeCreator.createBinarySearchTree(elements);
        binarySearchTree.toString();
    }

    /**
     * Test should successfully create Model 2 BinarySearchTree where second element is less than first element
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyCreateModel2BinarySearchTreeWhereSecondElementIsLessThanFirstElement() throws Exception {
        createModel2BinarySearchTree(10, 5);
    }

    /**
     * Test should successfully create Model 2 BinarySearchTree where second element is bigger than the first element.
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyCreateModel2BinarySearchTreeWhereSecondElementIsBiggerThanFirstElement() throws Exception {
        createModel2BinarySearchTree(10, 12);
    }
}
