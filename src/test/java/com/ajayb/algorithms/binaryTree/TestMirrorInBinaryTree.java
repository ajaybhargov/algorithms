package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * @author : ajay.b
 */
public class TestMirrorInBinaryTree {
    /**
     * Test should successfully check that the BinaryTree is not a mirror.
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyCheckThatTheBinaryTreeIsNotAMirror() throws Exception {
        Node node = new Node(10, null, null);
        node.insertToTheLeft(new Node(5, null, null));
        BinaryTree binaryTree = new BinaryTree(node);
        boolean isMirror = new MirrorInBinaryTree().isMirror(binaryTree);
        assertFalse(isMirror);
    }

    /**
     * Test should successfully check that the binaryTree is a Mirror of Itself.
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyCheckThatTheBinaryTreeIsAMirrorOfItself() throws Exception {
        Node node = new Node(10, null, null);
        node.insertToTheLeft(new Node(5, null, null));
        node.insertToTheRight(new Node(5, null, null));
        BinaryTree binaryTree = new BinaryTree(node);
        boolean isMirror = new MirrorInBinaryTree().isMirror(binaryTree);
        assertTrue(isMirror);
    }

    /**
     * Test should successfully check that the binary tree is a mirror of itself the depth of the node is 3
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyCheckThatTheBinaryTreeIsAMirrorOfItselfForAComplicatedScenario() throws Exception {
        Node node = new Node(10, null, null);
        Node leftNodeInLevelOne = new Node(5, null, null);
        node.insertToTheLeft(leftNodeInLevelOne);
        Node rightNodeInLevelOne = new Node(5, null, null);
        node.insertToTheRight(rightNodeInLevelOne);
        leftNodeInLevelOne.insertToTheLeft(new Node(8, null, null));
        leftNodeInLevelOne.insertToTheRight(new Node(6, null, null));
        rightNodeInLevelOne.insertToTheRight(new Node(8, null, null));
        rightNodeInLevelOne.insertToTheLeft(new Node(6, null, null));
        BinaryTree binaryTree = new BinaryTree(node);
        boolean isMirror = new MirrorInBinaryTree().isMirror(binaryTree);
        assertTrue(isMirror);
    }
}
