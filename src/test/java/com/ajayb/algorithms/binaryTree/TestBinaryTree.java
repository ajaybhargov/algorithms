package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;
import org.testng.annotations.Test;

/**
 * @author : ajay.b
 */
public class TestBinaryTree {

    @Test
    public void testShouldSuccessfullyCreateBinaryTree() throws Exception {
        Node node = new Node(10, null, null);
        node.insertToTheLeft(new Node(5, null, null));
        node.insertToTheRight(new Node(5, null, null));
        BinaryTree binaryTree = new BinaryTree(node);
        binaryTree.toString();
    }
}
