package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Responsible for printing all the paths in a tree
 *
 * @author ajay.b
 */
public class AllPathsOfATree {

    public void displayAllPaths(BinaryTree binaryTree) {
        ArrayList<ArrayList<Integer>> listOfAllPaths = new ArrayList<ArrayList<Integer>>();
        Node node = binaryTree.getNode();
        if (node != null) {
            displayAllPathsWithoutRecursion(node, listOfAllPaths);
        }
        System.out.println("listOfAllPaths = " + listOfAllPaths);
    }

    private void displayAllPathsWithoutRecursion(Node rootNode, ArrayList<ArrayList<Integer>> elementStack) {
        Node node = rootNode;
        Stack<Node> stack = new Stack<Node>();
        boolean shouldMoveBackFromCurrentNode = false;
        while (!(stack.empty() && rootNode.isTraversed())) {
            ArrayList<Integer> integerArrayList = new ArrayList<Integer>();
            if (shouldMoveBackFromCurrentNode) {
                populateAlreadyVisitedElementsForTheNewPath(stack, integerArrayList);
            }
            while (node != null && !node.isTraversed()) {
                integerArrayList.add(node.getValue());
                stack.push(node);
                Node childNode;
                if (!shouldMoveBackFromCurrentNode) {
                    childNode = node.getLeft();
                } else {
                    childNode = node.getRight();
                    node.setTraversed(true);
                }
                node = childNode;
                shouldMoveBackFromCurrentNode = false;
            }
            //pop the element from the stack since we have to move back to the parent.
            node = stack.pop();
            if (!node.isTraversed() && !checkForInternalNode(node)) {
                //adding it since its a new path.
                elementStack.add(integerArrayList);
            } else if (node.isTraversed()) {
                //pop the next node from the stack since we have already visited both the left and right children of the node.
                if (!stack.isEmpty()) {
                    node = stack.pop();
                }
            }
            shouldMoveBackFromCurrentNode = true;
        }
    }

    private void populateAlreadyVisitedElementsForTheNewPath(Stack<Node> stack, ArrayList<Integer> integerArrayList) {
        for (Node nodePresentInStack : stack) {
            integerArrayList.add(nodePresentInStack.getValue());
        }
    }

    private boolean checkForInternalNode(Node node) {
        return node.getLeft() != null && node.getRight() != null;
    }
}
