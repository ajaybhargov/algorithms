package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;

/**
 * Responsible for left rotation in a binary tree.
 */
public class LeftRotation {

    public void rotate(Node actualNode) {
        Integer tempValue = actualNode.getValue();
        Node rightNode = actualNode.getRight();
        Node leftNode = actualNode.getLeft();
        actualNode.setValue(rightNode.getValue());
        actualNode.setLeft(new Node(tempValue, leftNode, rightNode.getLeft()));
        actualNode.setRight(rightNode.getRight());
    }
}
