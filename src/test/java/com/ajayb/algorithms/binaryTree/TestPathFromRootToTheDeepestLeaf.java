package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTree;
import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTreeCreator;
import org.testng.annotations.Test;

import java.util.AbstractList;
import java.util.ArrayList;

/**
 * @author : ajay.b
 */
public class TestPathFromRootToTheDeepestLeaf {

    /**
     * Test should display all the elements present in the path from root to the deepest leaf.
     *
     * @throws Exception
     */
    @Test
    public void testShouldDisplayAllTheElementsPresentInThePathFromRootToTheDeepestLeaf() throws Exception {
        BinarySearchTree binarySearchTree = createBinarySearchTree();
        PathFromRootToTheDeepestLeaf pathFromRootToTheDeepestLeaf = new PathFromRootToTheDeepestLeaf();
        pathFromRootToTheDeepestLeaf.print(binarySearchTree);

    }

    private BinarySearchTree createBinarySearchTree() {
        ArrayList<Integer> listOfElements = new ArrayList<Integer>();
        listOfElements.add(10);
        listOfElements.add(5);
        listOfElements.add(14);
        listOfElements.add(15);
        listOfElements.add(11);
        listOfElements.add(13);
        BinarySearchTree binarySearchTree = BinarySearchTreeCreator.createBinarySearchTree(listOfElements);
        binarySearchTree.toString();
        return binarySearchTree;
    }

}
