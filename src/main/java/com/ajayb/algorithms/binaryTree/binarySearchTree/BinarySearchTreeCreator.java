package com.ajayb.algorithms.binaryTree.binarySearchTree;

import com.ajayb.algorithms.Node;

import java.util.List;

/**
 * @author : ajay.b
 */
public class BinarySearchTreeCreator {

    public static BinarySearchTree createBinarySearchTree(List<Integer> listOfElements) {
        BinarySearchTree binarySearchTree = null;
        for (int i = 0; i < listOfElements.size(); i++) {
            if (i == 0) {
                Node rootNode = new Node(listOfElements.get(i), null, null);
                binarySearchTree = new BinarySearchTree(rootNode);
            } else {
                binarySearchTree.insert(listOfElements.get(i));
            }
        }
        return binarySearchTree;
    }
}
