package com.ajayb.algorithms.binaryTree.binarySearchTree.model2;

import com.ajayb.algorithms.Node;
import com.ajayb.algorithms.binaryTree.BinaryTree;

/**
 * @author : ajay.b
 */
public class Model2BinarySearchTree extends BinaryTree {

    public Model2BinarySearchTree(Node node) {
        super(node);
        getNode().setLeft(new Node(node.getValue(), null, null));
    }

    public void insert(Integer valueToBeInserted) {
        insert(valueToBeInserted, getNode());
    }

    private void insert(Integer valueToBeInserted, Node node) {
        //stopping condition
        if (node.getRight() == null) {
            if (valueToBeInserted == node.getValue()) {
                throw new UnsupportedOperationException("Element is already present : " + valueToBeInserted);
            } else if (valueToBeInserted > node.getValue()) {
                //insert to the right
                Node copyOfTheCurrentNode = new Node(node.getValue(), node.getLeft(), node.getRight());
                node.setValue(valueToBeInserted);
                Node nodeToBeInsertedToTheRight = new Node(valueToBeInserted, new Node(valueToBeInserted, null, null), null);
                node.setRight(nodeToBeInsertedToTheRight);
                node.setLeft(copyOfTheCurrentNode);
            } else {
                //insert to the left since the valueToBeInserted is smaller
                Node copyOfTheCurrentNode = new Node(node.getValue(), node.getLeft(), node.getRight());
                Node nodeToBeInsertedToTheLeft = new Node(valueToBeInserted, new Node(valueToBeInserted, null, null), null);
                node.setLeft(nodeToBeInsertedToTheLeft);
                node.setRight(copyOfTheCurrentNode);
            }
            return;
        }

        if (valueToBeInserted < node.getValue()) {
            insert(valueToBeInserted, node.getLeft());
        } else {
            insert(valueToBeInserted, node.getRight());
        }
    }

}
