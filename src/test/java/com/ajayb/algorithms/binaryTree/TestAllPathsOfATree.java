package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTree;
import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTreeCreator;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class TestAllPathsOfATree {

    private BinarySearchTree createBinarySearchTree() {
        ArrayList<Integer> listOfElements = new ArrayList<Integer>();
        listOfElements.add(10);
        listOfElements.add(5);
        listOfElements.add(14);
        listOfElements.add(15);
        listOfElements.add(11);
        listOfElements.add(13);
        BinarySearchTree binarySearchTree = BinarySearchTreeCreator.createBinarySearchTree(listOfElements);
        binarySearchTree.toString();
        return binarySearchTree;
    }

    /**
     * Test should display all the paths of a tree without recursion
     *
     * @throws Exception
     */
    @Test
    public void testShouldDisplayAllPathsOfATreeWithoutRecursion() throws Exception {
        BinarySearchTree binarySearchTree = createBinarySearchTree();
        AllPathsOfATree allPathsOfATree = new AllPathsOfATree();
        allPathsOfATree.displayAllPaths(binarySearchTree);
    }

    @Test
    public void testShouldDisplayAllPathsOfATreeUsingRecursion() throws Exception {
        BinarySearchTree binarySearchTree = createBinarySearchTree();
        AllPathsOfATreeUsingRecursion allPathsOfATreeUsingRecursion = new AllPathsOfATreeUsingRecursion();
        allPathsOfATreeUsingRecursion.displayAllPaths(binarySearchTree);
    }
}
