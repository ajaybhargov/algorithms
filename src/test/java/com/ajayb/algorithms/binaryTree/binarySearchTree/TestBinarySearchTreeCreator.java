package com.ajayb.algorithms.binaryTree.binarySearchTree;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : ajay.b
 */
public class TestBinarySearchTreeCreator {

    /**
     * Test should successfully create a Binary Search Tree given a list of elements.
     *
     * @throws Exception
     */
    @Test
    public void testShouldCreateBinarySearchTreeSuccessfully() throws Exception {
        List<Integer> elements = new ArrayList<Integer>();
        elements.add(5);
        elements.add(10);
        elements.add(8);
        elements.add(1);
        elements.add(6);
        elements.add(4);
        BinarySearchTree binarySearchTree = BinarySearchTreeCreator.createBinarySearchTree(elements);
        binarySearchTree.toString();
    }
}
