package com.ajayb.algorithms.binaryTree.binarySearchTree.model2;

import com.ajayb.algorithms.Node;

import java.util.List;

/**
 * @author : ajay.b
 */
public class Model2BinarySearchTreeCreator {

    public static Model2BinarySearchTree createBinarySearchTree(List<Integer> listOfElements) {
        Model2BinarySearchTree binarySearchTree=null;
        for (int i = 0; i < listOfElements.size(); i++) {
            Integer elementToBeInserted = listOfElements.get(i);
            if (i == 0) {
                binarySearchTree = new Model2BinarySearchTree(new Node(elementToBeInserted, null, null));
            }else{
                binarySearchTree.insert(elementToBeInserted);
            }
        }
        return binarySearchTree;
    }
}
