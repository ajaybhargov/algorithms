package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.Node;

/**
 * @author : ajay.b
 */
public class MirrorInBinaryTree {
    public boolean isMirror(BinaryTree binaryTree) {
        Node root = binaryTree.getNode();
        if (root == null) {
            return true;
        }
        if (root.hasNoChildren()) {
            return true;
        }
        Node left = root.getLeft();
        Node right = root.getRight();
        return isMirror(left, right);
    }

    private boolean isMirror(Node tree1, Node tree2) {
        if (tree1 == null && tree2 != null) {
            return false;
        }
        if (tree1 != null && tree2 == null) {
            return false;
        }
        if (tree1 == null && tree2 == null) {
            return true;
        }
        if (tree1.getValue().equals(tree2.getValue()) &&
                isMirror(tree1.getLeft(), tree2.getRight()) &&
                isMirror(tree1.getRight(), tree2.getLeft())) {
            return true;
        } else {
            return false;
        }
    }

}
