package com.ajayb.algorithms.binaryTree;

import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTree;
import com.ajayb.algorithms.binaryTree.binarySearchTree.BinarySearchTreeCreator;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ajay.b
 */
public class TestLeftRotation {

    /**
     * Test should successfully left rotate a Simple binary tree
     *
     * @throws Exception
     */
    @Test
    public void testShouldSuccessfullyLeftRotateBinaryTree() throws Exception {
        ArrayList<Integer> listOfElements = new ArrayList<Integer>();
        listOfElements.add(20);
        listOfElements.add(15);
        listOfElements.add(10);
        listOfElements.add(18);
        listOfElements.add(19);
        BinarySearchTree binarySearchTree = BinarySearchTreeCreator.createBinarySearchTree(listOfElements);
        binarySearchTree.toString();
        LeftRotation leftRotation = new LeftRotation();
        leftRotation.rotate(binarySearchTree.getNode().getLeft());
        binarySearchTree.toString();
    }

    @Test
    public void testShouldSuccessfullyLeftRotateComplexBinaryTreeWhichIsLeftSkewed() throws Exception {
        List<Integer> listOfElements = new ArrayList<Integer>();
        listOfElements.add(25);
        listOfElements.add(15);
        listOfElements.add(10);
        listOfElements.add(18);
        listOfElements.add(16);
        listOfElements.add(22);
        listOfElements.add(21);
        listOfElements.add(24);
        BinarySearchTree binarySearchTree = BinarySearchTreeCreator.createBinarySearchTree(listOfElements);
        binarySearchTree.toString();
        LeftRotation leftRotation = new LeftRotation();
        leftRotation.rotate(binarySearchTree.getNode().getLeft());
        binarySearchTree.toString();
    }

    @Test
    public void testShouldSuccessfullyLeftRotateComplexBinaryTreeWhichIsRightSkewed() throws Exception {
        List<Integer> listOfElements = new ArrayList<Integer>();
        listOfElements.add(10);
        listOfElements.add(15);
        listOfElements.add(13);
        listOfElements.add(18);
        listOfElements.add(16);
        listOfElements.add(22);
        listOfElements.add(21);
        listOfElements.add(24);
        BinarySearchTree binarySearchTree = BinarySearchTreeCreator.createBinarySearchTree(listOfElements);
        binarySearchTree.toString();
        LeftRotation leftRotation = new LeftRotation();
        leftRotation.rotate(binarySearchTree.getNode().getRight());
        binarySearchTree.toString();

    }
}
