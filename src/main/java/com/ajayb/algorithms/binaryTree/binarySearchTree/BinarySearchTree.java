package com.ajayb.algorithms.binaryTree.binarySearchTree;

import com.ajayb.algorithms.Node;
import com.ajayb.algorithms.binaryTree.BinaryTree;

/**
 * @author : ajay.b
 */
public class BinarySearchTree extends BinaryTree {

    public BinarySearchTree(Node node) {
        super(node);
    }

    public void insert(Integer valueToBeInserted) {
        insert(valueToBeInserted, getNode());
    }

    private void insert(Integer valueToBeInserted, Node node) {
        if (valueToBeInserted > node.getValue() && node.getRight() != null) {
            insert(valueToBeInserted, node.getRight());
        } else if (valueToBeInserted > node.getValue() && node.getRight() == null) {
            Node nodeToBeInserted = new Node(valueToBeInserted, null, null);
            node.setRight(nodeToBeInserted);
        } else if (valueToBeInserted < node.getValue() && node.getLeft() != null) {
            insert(valueToBeInserted, node.getLeft());
        } else if (valueToBeInserted < node.getValue() && node.getLeft() == null) {
            Node nodeToBeInserted = new Node(valueToBeInserted, null, null);
            node.setLeft(nodeToBeInserted);
        } else if (valueToBeInserted.equals(node.getValue())) {
            throw new UnsupportedOperationException("Element is already present : " + valueToBeInserted);
        }
    }

}
