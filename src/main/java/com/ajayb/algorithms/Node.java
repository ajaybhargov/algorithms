package com.ajayb.algorithms;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author : ajay.b
 */
public class Node {
    private Integer value;
    private Node left;
    private Node right;
    private boolean traversed;

    public Node(Integer value, Node left, Node right) {
        this.setValue(value);
        this.setLeft(left);
        this.setRight(right);
        this.setTraversed(false);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public boolean hasNoChildren() {
        if (getLeft() == null && getRight() == null) {
            return true;
        } else {
            return false;
        }
    }

    public Integer getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public void insertToTheLeft(Node nodeToBeInserted) {
        if (getLeft() == null) {
            setLeft(nodeToBeInserted);
        } else {
            throw new UnsupportedOperationException("Element is already present to the left of the node");
        }
    }

    public void insertToTheRight(Node nodeToBeInserted) {
        if (getRight() == null) {
            setRight(nodeToBeInserted);
        } else {
            throw new UnsupportedOperationException("Element is already present to the right of the node");
        }
    }

    public void setTraversed(boolean traversed) {
        this.traversed = traversed;
    }

    public boolean isTraversed() {
        return traversed;
    }
}
